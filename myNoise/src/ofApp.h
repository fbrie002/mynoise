#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);
    
        float getZNoiseValue(int x, int y);

        ofCamera cam;
        ofMesh mesh;
        ofSpherePrimitive sphere;
        ofMesh ball;
    
        int numCols;
        int numRows;
    
        float a;
        float d;
        float t;
    
        float n1;
        float n2;
        float n1_freq;
        float n2_freq;
        float n1_amp;
        float n2_amp;
    
        float time;
        ofTexture texture;
        ofTexture texture2;
    
        ofLight lamp;
    
    
    
        
};
