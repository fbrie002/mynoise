#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofEnableDepthTest(); // enable depth test
    
    cam.setGlobalPosition({ 0, -200, getZNoiseValue(n1, n2) }); //global cam position
    
    //background
    ofSetBackgroundColor(24, 0, 0);
    
    //light
    lamp.enable();
    lamp.setPosition(ofVec3f(-20, -100, -30));
    lamp.lookAt(ofVec3f(20, 0, 0));

    //textures
    ofDisableArbTex();
    ofLoadImage(texture, "cell4.jpg");
    ofLoadImage(texture2, "cell2.jpg");
    
    //set columns and rows
    numCols = numRows = 100;
    
    d = 0;
    a = 0;
    t = 0;
    
    int size = 2;

    for (int x = -numCols / 2; x < numCols / 2; x++) {
        for (int y = -numRows / 2; y < numRows / 2; y++) {

            getZNoiseValue(x, y);
            mesh.addVertex(ofPoint(x*size, y*size, 0)); // make a new vertex
            mesh.addColor(ofFloatColor(255, 255, 255));  // add a color at the vertices
        }
    }

    // Set up triangles' indices§
    // only loop to -1 so they don't connect back to the beginning of the row
    for (int x = 0; x < numCols - 1; x++) {
        for (int y = 0; y < numRows - 1; y++) {
            int topLeft = x + numCols * y;
            int bottomLeft = x + 1 + numCols * y;
            int topRight = x + numCols * (y + 1);
            int bottomRight = x + 1 + numCols * (y + 1);

            mesh.addTriangle(topLeft, bottomLeft, topRight);
            mesh.addTriangle(bottomLeft, topRight, bottomRight);

        }
    }
}

//--------------------------------------------------------------
void ofApp::update(){

    for (int i = 0; i < mesh.getVertices().size(); i++){
        float x = mesh.getVertex(i).x;
        float y = mesh.getVertex(i).y;
        
        mesh.setVertex(i, ofVec3f(x, y, getZNoiseValue(x, y)));
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    cam.begin();
    
    //camera position controlled with keys
    cam.panDeg(a);
    cam.dolly(d);
    
    //box
    ofSetColor(250, 160, 160);
    ofDrawBox(0, 0, getZNoiseValue(0, 0) -4, 10); // draw 3d box
    
    //cells
    texture.bind(); //texture 1
    ofDrawSphere(20, 0, getZNoiseValue(20, 2), 4);
    ofDrawSphere(40, 10, getZNoiseValue(40, 10), 2);
    ofDrawSphere(-70, -60, getZNoiseValue(-70, -60), 14);
    ofDrawSphere(80, 90, getZNoiseValue(80, 90), 4);
    ofDrawSphere(-20, 50, getZNoiseValue(-20, 50), 4);
    texture.unbind();
    
    texture2.bind();    //texture 2
    ofDrawSphere(-80, 90, getZNoiseValue(-80, 90), 10);
    ofDrawSphere(-50, -30, getZNoiseValue(-50, -30), 7);
    ofDrawSphere(70, -60, getZNoiseValue(70, -60), 3);
    ofDrawSphere(40, -60, getZNoiseValue(40, -60), 1);
    texture2.unbind();
    
    //draw mesh
    mesh.drawWireframe();

    cam.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
    //zoom in
    if (key == 'w'){
        
        d -= 0.3;
    }
    //zoom out
    if (key == 'a'){
        
        d += 0.3;
    }
    //pan left
    if (key == 's'){

        a += 0.1;
    }
    //pan right
    if (key == 'd'){

        a -= 0.1;
    }
    //turn
    if (key == 't'){
        cam.tiltDeg(90);
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
    //zoom in
    if (key == 'w'){
        d = 0;
    }
    //zoom out
    if (key == 'a'){
        d = 0;
    }
    //pan left
    if (key == 's'){
        a = 0;
    }
    //pan right
    if (key == 'd'){
        a = 0;
    }

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
//noise function
float ofApp::getZNoiseValue(int x, int y) {

    n1_freq = 0.05;
    n2_freq = 0.03;
    n1_amp = 3;
    n2_amp = 5;
    
    time = ofGetElapsedTimef()/10;
    
    n1 = ofNoise(x * n1_freq, y * n1_freq + time) * n1_amp;
    n2 = ofNoise(x * n2_freq, y * n2_freq + time) * n2_amp;
    return n1 + n2;
}
